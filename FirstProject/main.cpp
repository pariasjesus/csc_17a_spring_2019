/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 18, 2019, 7:44 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

void shift(int*, int);
    
int main(int argc, char** argv) {
    int size = 5;
    int* array = new int[size];
    for(int n=0;n<size;n++){
        array[n]=n;
    }
    for(int n=0;n<size;n++){
        cout<< n << "   "<< array[n]<<endl;
    }
    cout<<endl;
    shift(array,size);
    for(int n=0;n<size;n++){
        cout<< n << "   "<< array[n]<<endl;
    }
    delete[] array;
    array=NULL;
    return 0;
}

void shift(int* array, int size){
    int temp;
    for(int n=size-1;n>=0;n--){
        temp = array[n];
        array[n+1]=array[n];
        array[n] = temp;
    }
}
